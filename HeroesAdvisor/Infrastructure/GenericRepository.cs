﻿using HeroesAdvisor.Domain;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace HeroesAdvisor.Infrastructure {
    public class GenericRepository<T> where T : class, IDbEntity, IActivatable {

        protected DbContext _db;

        protected IQueryable<T> Table {
            get {
                return _db.Set<T>();
            }
        }

        public GenericRepository(DbContext db) {
            _db = db;
        }

        public IQueryable<T> List() {
            return Table.Where(e => e.Active).Select(m => m);
        }

        public IQueryable<T> Get(int id) {
            return Table.Where(e => e.Active && e.Id == id).Select(m => m);
        }

        public void Add(T entity) {
            _db.Set<T>().Add(entity);
        }

        public void Delete(T entity) {
            entity.Active = false;
        }

        public void Delete(int id) {
            Delete(Get(id).FirstOrDefault());
        }

        public void SaveChanges() {
            try {
                _db.SaveChanges();
            } catch (DbEntityValidationException dbError) {
                var firstError = dbError.EntityValidationErrors.First().ValidationErrors.First().ErrorMessage;
                throw new ValidationException(firstError);
            }
        }

    }
}