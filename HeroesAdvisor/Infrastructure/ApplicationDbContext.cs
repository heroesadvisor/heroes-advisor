﻿using HeroesAdvisor.Domain;
using HeroesAdvisor.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace HeroesAdvisor.Infrastructure {
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {

        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false) { }

        public static ApplicationDbContext Create() {
            return new ApplicationDbContext();
        }

        public IDbSet<Hero> Heroes { get; set; }

        /// <summary>
        /// Abilities. Duh.
        /// </summary>
        public IDbSet<Ability> Abilities { get; set; }

        /// <summary>
        /// The statistics about a given hero
        /// </summary>
        public IDbSet<Statistics> Statistics { get; set; }

        /// <summary>
        /// The table of counters for heroes
        /// </summary>
        public IDbSet<Counter> Counters { get; set; }

        /// <summary>
        /// Table of synergies for all of the heroes
        /// </summary>
        public IDbSet<Synergy> Synergies { get; set; }

        //IF you're starting a new app you'll need to open the Package Manager Console and run 'enable-migrations'
        //for this app I have already run it.  You may have to run it for your machine, I am not sure.
        //
        // you may have to specify which file you wish to enable migrations from
        // e.g. To enable migrations for 'HeroesAdvisor.Infrastructure.ApplicationDbContext', use Enable-Migrations -ContextTypeName HeroesAdvisor.Infrastructure.ApplicationDbContext

        //After that you'll need to add-migration, this scaffolds the database based off the IDBSets in this file
        //you can think of these as commits, each one details changes to the EF db.  So since the first one is your
        //initial state you can do add-migration 'init' this will create a file like 234235235325235235_init.cs

        //once this is done, to finish establishing your changes do update-database
        //each time a migration is pull you will need to update-database on your machine.  You do NOT need to add-migrations if you're not the one who changed the domain models

        //when updating domain models, add a migration and then update database
    }
}