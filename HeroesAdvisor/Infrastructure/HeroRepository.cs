﻿using HeroesAdvisor.Domain;
using System.Data.Entity;

namespace HeroesAdvisor.Infrastructure {
    public class HeroRepository : GenericRepository<Hero> {

        public HeroRepository(DbContext db) : base (db) { }

    }
}