namespace HeroesAdvisor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStatisticsandEnumerablesforRolesandSubRolesRollyROlleh : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Statistics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhysicalArmor = c.Int(nullable: false),
                        SpellArmor = c.Int(nullable: false),
                        Mana = c.Int(nullable: false),
                        HitPoints = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Heroes", "Statistics_Id", c => c.Int());
            AlterColumn("dbo.Heroes", "Role", c => c.Int(nullable: false));
            AlterColumn("dbo.Heroes", "SubRole", c => c.Int(nullable: false));
            CreateIndex("dbo.Heroes", "Statistics_Id");
            AddForeignKey("dbo.Heroes", "Statistics_Id", "dbo.Statistics", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Heroes", "Statistics_Id", "dbo.Statistics");
            DropIndex("dbo.Heroes", new[] { "Statistics_Id" });
            AlterColumn("dbo.Heroes", "SubRole", c => c.String());
            AlterColumn("dbo.Heroes", "Role", c => c.String());
            DropColumn("dbo.Heroes", "Statistics_Id");
            DropTable("dbo.Statistics");
        }
    }
}
