namespace HeroesAdvisor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class interfaces : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Heroes", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Heroes", "Active");
        }
    }
}
