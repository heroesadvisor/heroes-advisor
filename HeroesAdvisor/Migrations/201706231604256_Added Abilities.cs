namespace HeroesAdvisor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAbilities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Abilities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Type = c.String(),
                        Active = c.Boolean(nullable: false),
                        Hero_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Heroes", t => t.Hero_Id)
                .Index(t => t.Hero_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Abilities", "Hero_Id", "dbo.Heroes");
            DropIndex("dbo.Abilities", new[] { "Hero_Id" });
            DropTable("dbo.Abilities");
        }
    }
}
