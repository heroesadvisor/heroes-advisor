namespace HeroesAdvisor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HeroesAdvisorInfrastructureApplicationDbContext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Counters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Active = c.Boolean(nullable: false),
                        CounterMagnitude = c.Int(nullable: false),
                        CounterHero_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Heroes", t => t.CounterHero_Id)
                .Index(t => t.CounterHero_Id);
            
            CreateTable(
                "dbo.Synergies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Active = c.Boolean(nullable: false),
                        SynergyMagnitude = c.Int(nullable: false),
                        HeroStats_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Heroes", t => t.HeroStats_Id)
                .Index(t => t.HeroStats_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Synergies", "HeroStats_Id", "dbo.Heroes");
            DropForeignKey("dbo.Counters", "CounterHero_Id", "dbo.Heroes");
            DropIndex("dbo.Synergies", new[] { "HeroStats_Id" });
            DropIndex("dbo.Counters", new[] { "CounterHero_Id" });
            DropTable("dbo.Synergies");
            DropTable("dbo.Counters");
        }
    }
}
