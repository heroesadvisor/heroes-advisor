// <auto-generated />
namespace HeroesAdvisor.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class interfaces : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(interfaces));
        
        string IMigrationMetadata.Id
        {
            get { return "201706231559076_interfaces"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
