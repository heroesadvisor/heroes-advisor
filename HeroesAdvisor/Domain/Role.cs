﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeroesAdvisor.Domain
{
    /// <summary>
    /// The main roles that any hero can assume
    /// </summary>
    public enum Role
    {
        Multiclass,
        Warrior,
        Support,
        Assassin,
        Specialist
    }

    /// <summary>
    /// The sub role within the main role that the hero can assume
    /// </summary>
    public enum SubRole
    {
        Bruiser,
        Tank,
        Support,
        Healer,
        Utility,
        Ambusher,
        Burst,
        Sustain,
        Siege
    }
}