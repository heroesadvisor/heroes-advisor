﻿namespace HeroesAdvisor.Domain {
    public interface IDbEntity {
        int Id { get; set; }
    }
}
