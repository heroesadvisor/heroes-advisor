﻿using System.Collections.Generic;

namespace HeroesAdvisor.Domain {
    public class Hero : IDbEntity, IActivatable {
        public int Id { get; set; }

        public bool Active { get; set; }
        public string Name { get; set; }

        public Statistics Statistics { get; set; }

        public Role Role { get; set; }

        public SubRole SubRole { get; set; }

        /// <summary>
        /// THe list of abilities this hero has
        /// </summary>
        public List<Ability> Abilities { get; set; }





    }
}