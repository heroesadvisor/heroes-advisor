﻿using System;

namespace HeroesAdvisor.Domain
{
    public class Statistics : IActivatable, IDbEntity
    {
        public int PhysicalArmor { get; set; }
        public int SpellArmor { get; set; }
        public int Mana { get; set; }
        public int HitPoints { get; set; }

        public int Id { get; set; }

        public bool Active { get; set; }
    }
}