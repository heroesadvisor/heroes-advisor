﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeroesAdvisor.Domain
{
    public class Ability : IActivatable, IDbEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }

        public bool Active
        {
            get; set;
        }

        public int Id
        {
            get; set;
        }
    }
}