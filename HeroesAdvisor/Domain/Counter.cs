﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeroesAdvisor.Domain
{
    /// <summary>
    /// The class that represents a counter for a hero
    /// </summary>
    public class Counter : IDbEntity, IActivatable
    {
        public int Id { get ; set ; }
        public bool Active { get; set; }

        /// <summary>
        /// The hero that's a counter
        /// </summary>
        public Hero CounterHero { get; set; }

        /// <summary>
        /// Represents how well this hero counters
        /// </summary>
        public int CounterMagnitude { get; set; }
    }
}