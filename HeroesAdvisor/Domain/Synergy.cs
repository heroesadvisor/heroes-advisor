﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeroesAdvisor.Domain
{
    /// <summary>
    /// The class that represents a synergy with the hero
    /// </summary>
    public class Synergy : IActivatable, IDbEntity
    {
        public int Id { get; set; }
        public bool Active { get; set; }

        /// <summary>
        /// The hero that's synergistic with the hero
        /// </summary>
        public Hero HeroStats { get; set; }

        /// <summary>
        /// The effectiveness of this synergy
        /// </summary>
        public int SynergyMagnitude { get; set; }


    }
}