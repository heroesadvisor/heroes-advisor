﻿namespace HeroesAdvisor.Domain {
    public interface IActivatable {
        bool Active { get; set; }
    }
}
